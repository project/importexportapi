<?php

/**
 * @file
 * Implements data definitions on behalf of system.module.
 */

/**
 * Implementation of hook_def().
 */
function system_def() {
  $defs = array();

  $def = array(
    '#type' => 'entity',
    '#title' => t('Variable'),
    '#xml_plural' => 'variables',
    '#csv_plural' => 'variables'
  );

  $def['name'] = array(
    '#title' => t('Variable name'),
    '#key_component' => TRUE,
    '#unique' => TRUE
  );
  $def['value'] = array(
    '#type' => 'serialized',
    '#title' => t('Variable value')
  );

  $defs['variable'] = $def;

  $def = array(
    '#type' => 'entity',
    '#title' => t('File'),
    '#db_default_table' => 'files',
    '#xml_plural' => 'files',
    '#csv_plural' => 'files'
  );

  $def['fid'] = array(
    '#type' => 'int',
    '#title' => t('File ID'),
    '#key' => TRUE,
    '#db_uses_sequences' => TRUE
  );
  $def['nid'] = array(
    '#type' => 'int',
    '#title' => t('Node ID'),
    '#reference_entity' => 'node'
  );
  $def['file_name'] = array(
    '#title' => t('File name'),
    '#db_field_unaliased' => 'filename'
  );
  $def['file_path'] = array(
    '#type' => 'file',
    '#title' => t('File path'),
    '#db_field_unaliased' => 'filepath'
  );
  $def['file_mime'] = array(
    '#title' => t('File MIME type'),
    '#db_field_unaliased' => 'filemime'
  );
  $def['file_size'] = array(
    '#title' => t('File size'),
    '#db_field_unaliased' => 'filesize'
  );

  $def['file_revisions'] = array(
    '#type' => 'array',
    '#title' => t('File revisions'),
    '#xml_plural' => 'file-revisions',
    '#csv_plural' => 'file-revisions',
    '#xml_mapping' => 'file-revision'
  );
  $def['file_revisions']['fid'] = array(
    '#type' => 'int',
    '#title' => t('File ID'),
    '#reference_entity' => 'file'
  );
  $def['file_revisions']['vid'] = array(
    '#type' => 'int',
    '#title' => t('Node revision ID'),
    '#reference_entity' => 'node',
    '#reference_field' => array('revisions', 'vid')
  );
  $def['file_revisions']['description'] = array(
    '#title' => t('Description')
  );
  $def['file_revisions']['list'] = array(
    '#type' => 'int',
    '#title' => t('Show in list')
  );

  $defs['file'] = $def;

  return $defs;
}

/**
 * Implementation of hook_db_def_tables().
 */
function system_db_def_tables() {
  $tables = array();

  $tables['files'] = 'f';
  $tables['file_revisions'] = 'fr';
  $tables['variable'] = 'va';

  return $tables;
}
